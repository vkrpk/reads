<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testProperties(): void
    {
        $user = new User();
        $userBook = new UserBook();

        $user->setEmail('test@example.com')
            ->setRoles(['ROLE_ADMIN'])
            ->setPassword('test_password')
            ->setPseudo('TestUser')
            ->addUserBook($userBook);

        $this->assertEquals('test@example.com', $user->getEmail());
        $this->assertContains('ROLE_ADMIN', $user->getRoles());
        $this->assertContains('ROLE_USER', $user->getRoles());  // because every user at least has ROLE_USER
        $this->assertEquals('test_password', $user->getPassword());
        $this->assertEquals('TestUser', $user->getPseudo());
        $this->assertCount(1, $user->getUserBooks());

        $user->removeUserBook($userBook);
        $this->assertCount(0, $user->getUserBooks());
    }
}
