<?php

namespace App\Tests\Entity;

use App\Entity\Book;
use App\Entity\User;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class EntityIdTest extends TestCase
{
    /**
     * @dataProvider entitiesProvider
     */
    public function testEntityIdIsNullByDefault($entityClass): void
    {
        $entity = new $entityClass();
        $this->assertNull($entity->getId());
    }

    public function entitiesProvider(): array
    {
        return [
            [Book::class],
            [User::class],
            [UserBook::class],
        ];
    }
}