<?php

namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
    public function testCreateAndSetProperties(): void
    {
        $book = new Book();

        $book->setTitle('Sample Title');
        $book->setGoogleBooksId('sampleID1234');
        $book->setSubtitle('Sample Subtitle');
        $book->setDescription('Sample Description');
        $book->setIsbn10('1234567890');
        $book->setIsbn13('1234567890123');
        $book->setPageCount(250);
        $book->setSmallThumbnail('path/to/smallThumbnail.jpg');
        $book->setThumbnail('path/to/thumbnail.jpg');
        $book->setPublishDate(new \DateTime('2021-09-10'));

        $this->assertEquals('Sample Title', $book->getTitle());
        $this->assertEquals('sampleID1234', $book->getGoogleBooksId());
        $this->assertEquals('Sample Subtitle', $book->getSubtitle());
        $this->assertEquals('Sample Description', $book->getDescription());
        $this->assertEquals('1234567890', $book->getIsbn10());
        $this->assertEquals('1234567890123', $book->getIsbn13());
        $this->assertEquals(250, $book->getPageCount());
        $this->assertEquals('path/to/smallThumbnail.jpg', $book->getSmallThumbnail());
        $this->assertEquals('path/to/thumbnail.jpg', $book->getThumbnail());
        $this->assertEquals(new \DateTime('2021-09-10'), $book->getPublishDate());
    }

    public function testAuthors(): void
    {
        $book = new Book();
        $author = new Author();

        $book->addAuthor($author);

        $this->assertCount(1, $book->getAuthors());
        $this->assertTrue($book->getAuthors()->contains($author));

        $book->removeAuthor($author);
        $this->assertCount(0, $book->getAuthors());
    }

    public function testPublishers(): void
    {
        $book = new Book();
        $publisher = new Publisher();

        $book->addPublisher($publisher);

        $this->assertCount(1, $book->getPublishers());
        $this->assertTrue($book->getPublishers()->contains($publisher));

        $book->removePublisher($publisher);
        $this->assertCount(0, $book->getPublishers());
    }

    public function testUserBooks(): void
    {
        $book = new Book();
        $userBook = new UserBook();

        $book->addUserBook($userBook);
        $this->assertCount(1, $book->getUserBooks());
        $this->assertTrue($book->getUserBooks()->contains($userBook));

        $book->removeUserBook($userBook);
        $this->assertCount(0, $book->getUserBooks());
    }
}
