<?php

namespace App\Tests\Entity\Trait;

use App\Entity\Author;
use App\Entity\Publisher;
use App\Entity\Status;
use PHPUnit\Framework\TestCase;

class IdNameTraitTest extends TestCase
{
    /**
     * @dataProvider entitiesProvider
     */
    public function testTraitEntityIdIsNullByDefault($entityClass): void
    {
        $entity = new $entityClass();
        $this->assertNull($entity->getId());
    }

    /**
     * @dataProvider entitiesProvider
     */
    public function testTraitEntityNameSetterAndGetter($entityClass): void
    {
        $entity = new $entityClass();
        $entity->setName('John Doe');
        $this->assertEquals('John Doe', $entity->getName());
    }

    public function entitiesProvider(): array
    {
        return [
            [Status::class],
            [Author::class],
            [Publisher::class]
        ];
    }
}