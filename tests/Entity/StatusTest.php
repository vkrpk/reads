<?php

namespace App\Tests\Entity;

use App\Entity\Status;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function testUserBooks(): void
    {
        $status = new Status();
        $userBook = new UserBook();

        $status->addUserBook($userBook);
        $this->assertCount(1, $status->getUserBooks());
        $this->assertTrue($status->getUserBooks()->contains($userBook));

        $status->removeUserBook($userBook);
        $this->assertCount(0, $status->getUserBooks());
    }
}
