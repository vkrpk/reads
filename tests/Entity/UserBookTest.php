<?php

namespace App\Tests\Entity;

use App\Entity\Book;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class UserBookTest extends TestCase
{
    public function testProperties(): void
    {
        $userBook = new UserBook();
        $book = new Book();
        $user = new User();
        $status = new Status();
        $date = new \DateTimeImmutable();

        $userBook->setCreatedAt($date)
            ->setUpdatedAt($date)
            ->setComment('Test comment')
            ->setRating(5)
            ->setReader($user)
            ->setBook($book)
            ->setStatus($status);

        $this->assertEquals($date, $userBook->getCreatedAt());
        $this->assertEquals($date, $userBook->getUpdatedAt());
        $this->assertEquals('Test comment', $userBook->getComment());
        $this->assertEquals(5, $userBook->getRating());
        $this->assertEquals($user, $userBook->getReader());
        $this->assertEquals($book, $userBook->getBook());
        $this->assertEquals($status, $userBook->getStatus());
    }
}
