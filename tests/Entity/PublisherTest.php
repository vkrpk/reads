<?php

namespace App\Tests\Entity;

use App\Entity\Book;
use App\Entity\Publisher;
use PHPUnit\Framework\TestCase;

class PublisherTest extends TestCase
{
    public function testBooks(){
        $publisher = new Publisher();
        $book1 = $this->createMock(Book::class);
        $book2 = $this->createMock(Book::class);
        $publisher->addBook($book1);
        $publisher->addBook($book2);
        $this->assertCount(2, $publisher->getBooks());
        $this->assertTrue($publisher->getBooks()->contains($book1));
        $this->assertTrue($publisher->getBooks()->contains($book2));
        $publisher->removeBook($book1);
        $publisher->removeBook($book2);
        $this->assertCount(0, $publisher->getBooks());
        $this->assertFalse($publisher->getBooks()->contains($book1));
        $this->assertFalse($publisher->getBooks()->contains($book2));
    }
}
