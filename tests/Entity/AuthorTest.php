<?php

namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testGetId(): void
    {
        $author = new Author();
        $this->assertNull($author->getId());
    }
    public function testAddRemoveBook(): void
    {
        $author = new Author();
        $book = $this->createMock(Book::class);  // Create a mock of Book

        // Setup expectation that the addAuthor method of the Book object will be called once
        $book->expects($this->once())
            ->method('addAuthor')
            ->with($this->equalTo($author));

        $author->addBook($book);

        $this->assertCount(1, $author->getBooks());
        $this->assertTrue($author->getBooks()->contains($book));

        // Setup expectation that the removeAuthor method of the Book object will be called once
        $book->expects($this->once())
            ->method('removeAuthor')
            ->with($this->equalTo($author));

        $author->removeBook($book);

        $this->assertCount(0, $author->getBooks());
        $this->assertFalse($author->getBooks()->contains($book));
    }
}
