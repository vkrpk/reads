<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GoogleBooksApiService
{
    public function __construct(
        private readonly HttpCLientInterface $googlebooksClient,
    )
    {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function search(string $search): array
    {
        if(strlen($search) < 3) {
            return [];
        }

        return $this->makeRequest('GET', 'volumes', [
            'query' => [
                'q' => $search,
            ],
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     */
    private function makeRequest(string $method, string $url, array $options): array
    {
       $response = $this->googlebooksClient->request($method, $url, $options);
       return $response->toArray();
    }
}